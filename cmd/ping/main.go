package main

import (
	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_minecraft/minecraft"
)

func main() {
	mc, err := minecraft.New("127.0.0.1", 25565)
	if err != nil {
		log.Error(err)
		return
	}
	defer mc.Close()

	if err = mc.Handshake(1); err != nil {
		log.Error(err)
		return
	}

	if err = mc.Request(); err != nil {
		log.Error(err)
		return
	}

	resp, err := mc.Response()
	if err != nil {
		log.Error(err)
		return
	}

	log.Info(resp)

	if err = mc.Ping(); err != nil {
		log.Error(err)
		return
	}

	resp, err = mc.Response()
	if err != nil {
		log.Error(err)
		return
	}

	log.Info(resp)
}
