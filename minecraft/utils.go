package minecraft

import (
	"bytes"
	"encoding/binary"
)

// Add size of data to packet and send it
func (me *MC) send(data []byte) (n int, err error) {
	// get size of packet
	packet := new(bytes.Buffer)

	binary.Write(packet, binary.LittleEndian, varInt(len(data))) // Write size of packet
	packet.Write(data)                                           // Add data

	n, err = me.c.Write(packet.Bytes())
	if err != nil {
		return n, err
	}

	return n, nil
}
