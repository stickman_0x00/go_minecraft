package minecraft

import (
	"bytes"
	"encoding/binary"

	log "gitlab.com/stickman_0x00/go_log"
)

// https://wiki.vg/Protocol#Login_Start

func (me *MC) Login_start(username string) error {
	packet := new(bytes.Buffer)

	binary.Write(packet, binary.LittleEndian, varInt(0)) // Packet ID
	packet.Write(varString(username))                    // Name

	log.Debugf("Login: % x - %d", packet.Bytes(), len(packet.Bytes()))

	if _, err := me.send(packet.Bytes()); err != nil {
		return err
	}

	return nil
}
