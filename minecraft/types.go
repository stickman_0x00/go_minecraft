package minecraft

import (
	"bytes"
	"encoding/binary"
)

const (
	SEGMENT_BITS int = 0x7F
	CONTINUE_BIT int = 0x80
)

func varInt(value int) []byte {
	b := new(bytes.Buffer)

	for {
		if (value & ^SEGMENT_BITS) == 0 {
			binary.Write(b, binary.LittleEndian, int8(value))
			break
		}

		binary.Write(b, binary.LittleEndian, int8((value&SEGMENT_BITS)|CONTINUE_BIT))
		// Note: >>> means that the sign bit is shifted with the rest of the number rather than being left alone
		value = value >> 7
	}

	return b.Bytes()
}

func varString(s string) []byte {
	b := new(bytes.Buffer)

	binary.Write(b, binary.LittleEndian, varInt(len(s))) // Size of String
	b.Write([]byte(s))

	return b.Bytes()
}
