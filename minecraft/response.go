package minecraft

// https://wiki.vg/Protocol#Response

func (me *MC) Response() (string, error) {
	b := make([]byte, 2097151)

	if _, err := me.c.Read(b); err != nil {
		return "", err
	}

	return string(b), nil
}
