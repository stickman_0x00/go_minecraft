package minecraft

import (
	"bytes"
	"encoding/binary"

	log "gitlab.com/stickman_0x00/go_log"
)

// https://wiki.vg/Protocol#Handshake

// State
// 1- Ping
// 2 - Login
func (me *MC) Handshake(state int) error {
	packet := new(bytes.Buffer)

	binary.Write(packet, binary.LittleEndian, varInt(0))     // Packet ID
	binary.Write(packet, binary.LittleEndian, varInt(758))   // Protocol version
	packet.Write(varString(me.ip))                           // Server Address
	binary.Write(packet, binary.BigEndian, uint16(me.port))  // Server Port
	binary.Write(packet, binary.LittleEndian, varInt(state)) // Next State

	log.Debugf("Handshake: % x - %d", packet.Bytes(), len(packet.Bytes()))

	if _, err := me.send(packet.Bytes()); err != nil {
		return err
	}

	return nil

}
