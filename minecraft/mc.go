package minecraft

import (
	"net"
	"strconv"
)

// Minecraft connection
type MC struct {
	c net.Conn

	ip   string
	port int
}

func New(ip string, port int) (*MC, error) {

	c, err := net.Dial("tcp", ip+":"+strconv.Itoa(port))
	if err != nil {
		return nil, err
	}

	return &MC{
		c:    c,
		ip:   ip,
		port: port,
	}, nil
}

func (me *MC) Close() {

	me.c.Close()
}
